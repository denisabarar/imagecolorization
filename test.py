import os

import cv2
import numpy as np

import config
import model as model_builder
import data_generator
import matplotlib.pyplot as plt


def load_model():
    model_weights_path = os.path.join(os.getcwd(),
                                      config.DIR_SAVED_MODELS,
                                      'model.01-16773.0762.hdf5')

    m_b = model_builder.MyModel()
    print(m_b.model.summary())
    m_b.model.load_weights(model_weights_path)
    return m_b.model


# to do plot one img with orig, gray, pred
def demo():
    model = load_model()
    dir_name = os.path.join(os.getcwd(), config.DIR_VALID)
    file_name_list = os.listdir(dir_name)
    print(file_name_list)
    i = 0
    for file_name in file_name_list:
        file_name_path = os.path.join(dir_name, file_name)
        print(file_name_path)
        img_rgb_orig, img_rgb_bw, img_l, img_ab = data_generator.read_img(file_name_path)

        if img_l is None and img_ab is None:
            continue

        img_l_input = np.expand_dims(img_l, axis=0)

        # predict
        img_ab_pred = model.predict(img_l_input)

        img_lab_pred = np.concatenate((img_l, img_ab_pred[0]), axis=2)

        img_rgb_pred = cv2.cvtColor(img_lab_pred, cv2.COLOR_LAB2BGR)
        img_rgb_pred = img_rgb_pred.astype(np.uint8)

        cv2.imwrite(config.DIR_PRED + '{}_pred.png'.format(i), img_rgb_pred)
        cv2.imwrite(config.DIR_PRED + '{}_orig.png'.format(i), img_rgb_orig)
        cv2.imwrite(config.DIR_PRED + '{}_gray.png'.format(i), img_rgb_bw)
        i = i + 1


# def test():
#     model = load_model()
#     img_l, img_ab = data_generator.DataGenSequence(config.TEST)
#
#     img_ab_pred = model.predict_generator(img_l)
#
#     img_lab_pred = np.concatenate((img_l[0], img_ab_pred[0]), axis=2)
#
#     img_rgb_pred = cv2.cvtColor(img_lab_pred, cv2.COLOR_LAB2BGR)
#     img_rgb_pred = img_rgb_pred.astype(np.uint8)
#
#     cv2.imwrite(config.DIR_PRED + '{}_out.png'.format(i), img_rgb_pred)


if __name__ == '__main__':
    demo()
    # test()
