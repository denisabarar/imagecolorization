import tensorflow.compat.v2 as tfc

import config


class MyModel(tfc.keras.Model):
    def __init__(self):
        super(MyModel, self).__init__()
        self.kernel = config.KERNEL_SIZE
        self.batch_normalization1 = tfc.keras.layers.BatchNormalization()
        self.batch_normalization2 = tfc.keras.layers.BatchNormalization()
        self.batch_normalization3 = tfc.keras.layers.BatchNormalization()
        self.batch_normalization4 = tfc.keras.layers.BatchNormalization()
        self.batch_normalization5 = tfc.keras.layers.BatchNormalization()
        self.batch_normalization6 = tfc.keras.layers.BatchNormalization()
        self.batch_normalization7 = tfc.keras.layers.BatchNormalization()
        self.batch_normalization8 = tfc.keras.layers.BatchNormalization()
        self.batch_normalization9 = tfc.keras.layers.BatchNormalization()
        self.batch_normalization10 = tfc.keras.layers.BatchNormalization()
        self.batch_normalization11 = tfc.keras.layers.BatchNormalization()
        self.batch_normalization12 = tfc.keras.layers.BatchNormalization()
        self.batch_normalization13 = tfc.keras.layers.BatchNormalization()
        self.batch_normalization14 = tfc.keras.layers.BatchNormalization()
        self.batch_normalization15 = tfc.keras.layers.BatchNormalization()
        self.conv_64 = tfc.keras.layers.Conv2D(64, self.kernel, activation='relu', kernel_initializer="glorot_uniform", name='low_1')
        self.conv_128 = tfc.keras.layers.Conv2D(128, self.kernel, activation='relu', padding='same',
                                                kernel_initializer="glorot_uniform", name='low_2')
        self.conv_128_s2 = tfc.keras.layers.Conv2D(128, self.kernel, activation='relu',
                                                   kernel_initializer="glorot_uniform", strides=(2, 2), name='low_3')
        self.conv_256 = tfc.keras.layers.Conv2D(256, self.kernel, activation='relu', padding='same',
                                                kernel_initializer="glorot_uniform", name='low_4')

        self.conv_256_m = tfc.keras.layers.Conv2D(256, (self.kernel, self.kernel), activation='relu',
                                                  kernel_initializer="glorot_uniform", name='med_1')
        self.conv_128_m = tfc.keras.layers.Conv2D(128, (self.kernel, self.kernel), activation='relu', padding='same',
                                                  kernel_initializer="glorot_uniform", name='med_2')

        self.conv256_2_g = tfc.keras.layers.Conv2D(256, (self.kernel, self.kernel), activation='relu',
                                                   kernel_initializer="glorot_uniform", strides=(2, 2), name='glob_1')
        self.conv256_g = tfc.keras.layers.Conv2D(256, (self.kernel, self.kernel), activation='relu', padding='same',
                                                 kernel_initializer="glorot_uniform", strides=(1, 1), name='glob_2')
        self.conv256_2_g_2 = tfc.keras.layers.Conv2D(256, (self.kernel, self.kernel), activation='relu',
                                                     kernel_initializer="glorot_uniform", strides=(2, 2), name='glob_3')
        self.conv256_g_2 = tfc.keras.layers.Conv2D(256, (self.kernel, self.kernel), activation='relu', padding='same',
                                                   kernel_initializer="glorot_uniform", name='glob_4')
        self.flatten = tfc.keras.layers.Flatten(name='glob_5')
        self.fc1 = tfc.keras.layers.Dense(units=512, activation='relu', kernel_initializer='glorot_uniform', name='glob_6')
        self.fc2 = tfc.keras.layers.Dense(units=256, activation='relu', kernel_initializer='glorot_uniform', name='glob_7')
        self.fc3 = tfc.keras.layers.Dense(units=128, activation='relu', kernel_initializer='glorot_uniform', name='glob_8')

        self.deconv1 = tfc.keras.layers.Conv2DTranspose(128, (self.kernel, self.kernel), activation='relu',
                                                        padding='same',
                                                        name='deconv1',
                                                        kernel_initializer="glorot_uniform")
        self.deconv2 = tfc.keras.layers.Conv2DTranspose(64, (self.kernel, self.kernel), activation='relu',
                                                        name='deconv2',
                                                        kernel_initializer="glorot_uniform", strides=(2, 2))
        self.deconv3 = tfc.keras.layers.Conv2DTranspose(64, (self.kernel, self.kernel), activation='relu',
                                                        padding='same',
                                                        name='deconv3',
                                                        kernel_initializer="glorot_uniform")
        self.deconv4 = tfc.keras.layers.Conv2DTranspose(32, (self.kernel, self.kernel), activation='relu',
                                                        name='deconv4',
                                                        kernel_initializer="glorot_uniform", strides=(2, 2))
        self.deconv5 = tfc.keras.layers.Conv2DTranspose(2, (self.kernel, self.kernel), activation='tanh',
                                                        name='deconv5',
                                                        kernel_initializer="glorot_uniform", strides=(2, 2))

    def call(self, inputs, training=None, **kwargs):
        # low level Features Network
        x = self.conv_64(inputs)
        x = self.batch_normalization1(x)
        x = self.conv_128(x)
        x = self.batch_normalization2(x)
        x = self.conv_128_s2(x)
        x = self.batch_normalization3(x)
        x = self.conv_256(x)
        x = self.batch_normalization4(x)

        # Mid Level Features Network
        m = x
        xm = self.conv_256_m(m)
        xm = self.batch_normalization5(xm)
        xm = self.conv_128_m(xm)
        xm = self.batch_normalization6(xm)

        # Global Level Features Network
        x = self.conv256_2_g(x)
        x = self.batch_normalization7(x)
        x = self.conv256_g(x)
        x = self.batch_normalization8(x)
        x = self.conv256_2_g_2(x)
        x = self.batch_normalization9(x)
        x = self.conv256_g_2(x)
        x = self.batch_normalization10(x)
        x = self.flatten(x)
        x = self.fc1(x)
        x = self.fc2(x)
        x = self.fc3(x)

        # Fuse Layer
        xm_shape = xm.shape
        reshaped_global = x[:, tfc.newaxis, :]
        reshaped_global = tfc.tile(reshaped_global, [1, xm_shape[1] * xm_shape[2], 1])
        reshaped_global = tfc.reshape(reshaped_global, shape=[xm_shape[0], xm_shape[1], xm_shape[2], -1])
        x = tfc.concat([xm, reshaped_global], axis=3)

        # Colorization
        x = self.deconv1(x)
        x = self.batch_normalization11(x)
        x = self.deconv2(x)
        x = self.batch_normalization12(x)
        x = self.deconv3(x)
        x = self.batch_normalization13(x)
        x = self.deconv4(x)
        x = self.batch_normalization14(x)
        x = self.deconv5(x)
        x = self.batch_normalization15(x)
        return x


if __name__ == '__main__':
    modelImgColorization = MyModel()
    modelImgColorization.build(input_shape=(config.BATCH_SIZE, config.IMAGE_SIZE, config.IMAGE_SIZE, 1))
    print(modelImgColorization.summary())
