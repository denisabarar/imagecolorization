import os

import tensorflow.compat.v2 as tfc

import config as config
import data_generator as data_generator
import model as model_builder


def train(model):
    checkpoint_models_path = os.path.join(os.getcwd(), 'saved_models//')
    print(checkpoint_models_path)

    # Callbacks
    # tensor_board = tfc.keras.callbacks.TensorBoard(log_dir=Path('./logs'), histogram_freq=0, write_graph=True,
    #                                            write_images=True)
    # model_names = checkpoint_models_path + 'model.{epoch:02d}-{val_loss:.4f}.hdf5'
    # print(model_names)
    # model_checkpoint = tfc.keras.ModelCheckpoint(model_names, monitor='val_loss', verbose=1, save_best_only=True)
    # early_stop = EarlyStopping('val_loss', patience=config.PATIENCE)
    # reduce_lr = ReduceLROnPlateau('val_loss', factor=0.1, patience=int(config.PATIENCE / 4), verbose=1)

    # class MyCbk(keras.callbacks.Callback):
    #     def __init__(self, model):
    #         keras.callbacks.Callback.__init__(self)
    #         self.model_to_save = model
    #
    #     def on_epoch_end(self, epoch, logs=None):
    #         fmt = checkpoint_models_path + 'model.%02d-%.4f.hdf5'
    #         self.model_to_save.save(fmt % (epoch, logs['val_loss']))

    sgd = tfc.keras.optimizers.SGD(lr=0.001, momentum=0.9, nesterov=True, clipnorm=5.)
    model.compile(optimizer=sgd, loss='mean_squared_error')

    # Final callbacks
    # callbacks = [tensor_board, model_checkpoint, early_stop, reduce_lr]

    # Start Fine-tuning
    model.fit_generator(data_generator.DataGenSequence(config.TRAIN),
                        steps_per_epoch=config.NUM_TRAINING_SAMPLES // config.BATCH_SIZE,
                        validation_data=data_generator.DataGenSequence(config.VALID),
                        validation_steps=config.NUM_VALIDATION_SAMPLES // config.BATCH_SIZE,
                        epochs=config.EPOCHS,
                        verbose=1,
                        # callbacks=callbacks,
                        use_multiprocessing=True,
                        workers=4)
    print(model.evaluate_generator)


if __name__ == '__main__':
    model = model_builder.MyModel()
    print(model.summary())
    train(model)
