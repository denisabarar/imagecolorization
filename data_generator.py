import math
import os

import cv2
import numpy as np
import tensorflow.compat.v2 as tfc

import config as config


class DataGenSequence(tfc.keras.utils.Sequence):
    def __init__(self, usage):
        self.usage = usage
        if self.usage == config.TRAIN:
            self.dir_name = config.DIR_TRAIN
            self.batch_size = config.BATCH_SIZE
        elif self.usage == config.VALID:
            self.dir_name = config.DIR_VALID
            self.batch_size = config.BATCH_SIZE
        elif self.usage == config.TEST:
            self.dir_name = config.DIR_VALID
            self.batch_size = 1

        self.dir_path = os.path.join(os.getcwd(), self.dir_name)
        self.file_list = os.listdir(self.dir_path)
        self.size = len(self.file_list)

    def __len__(self):
        return math.ceil(self.size / self.batch_size)

    def __getitem__(self, idx):
        batch_idx = idx * self.batch_size  # batch index start
        batch_length = min(self.batch_size, self.size - batch_idx)

        train_images_l = []  # X
        train_images_ab = []  # Y

        for i in range(batch_length):
            file_name = os.path.join(self.dir_path, self.file_list[batch_idx])
            if os.path.isfile(file_name):
                _, _, img_l, img_ab = read_img(file_name)
                train_images_l.append(img_l)
                train_images_ab.append(img_ab)
        # Convert from list to np.array
        x_df = np.asarray(train_images_l)
        y_df = np.asarray(train_images_ab)
        print(x_df.shape)
        print(y_df.shape)
        x_df = tfc.dtypes.cast(x_df, tfc.float32)
        y_df = tfc.dtypes.cast(y_df, tfc.float32)
        return x_df, y_df


def read_img(file_name):
    # Load original RGB image
    img_rgb_orig = cv2.imread(file_name)
    img_rgb_bw = None
    img_l = None
    img_ab = None

    # Skip if is gray
    if not is_gray(img_rgb_orig):
        # Convert RGB to L*a*b
        img_lab = cv2.cvtColor(img_rgb_orig, cv2.COLOR_RGB2LAB)

        # Get the BW version
        img_lab_bw = img_lab.copy()
        img_lab_bw[:, :, 1:] = 0
        img_rgb_bw = cv2.cvtColor(img_lab_bw, cv2.COLOR_LAB2BGR)

        # Extract the L - luminance
        img_l = img_lab[:, :, 0]
        img_ab = img_lab[:, :, 1:]
        img_l = np.reshape(img_l, (config.IMAGE_SIZE, config.IMAGE_SIZE, 1))

    return img_rgb_orig, img_rgb_bw, img_l, img_ab


def is_gray(img):
    if len(img.shape) < 3:
        return True
    if img.shape[2] == 1:
        return True
    b, g, r = img[:, :, 0], img[:, :, 1], img[:, :, 2]
    # if (b == g).any() and (b == r).all():
    if np.array_equal(b, g) and np.array_equal(b, r):
        return True
    return False


if __name__ == '__main__':
    d = DataGenSequence(config.TRAIN)
    x, y = d.__getitem__(1)
    print(x.shape)
    print(y.shape)

